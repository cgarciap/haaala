/*
 * Copyright 2010 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.android;

import com.facebook.android.BaseRequestListener;
import com.facebook.android.Facebook.DialogListener;
import com.cgpcosmad.hala.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;

public class LoginButton extends ImageButton implements View.OnClickListener {
    
    private Facebook mFb;
    private Handler mHandler;
    private String[] mPermissions;
    private Activity mActivity;
    private SessionEvents events;
    
    public LoginButton(Context context) {
        super(context);
        this.events = new SessionEvents();
    }
    
    public LoginButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.events = new SessionEvents();
    }
    
    public LoginButton(Context context, AttributeSet attrs, int defStyle, SessionListener mSessionListener) {
        super(context, attrs, defStyle);
        this.events = new SessionEvents();
    }
    
    public void init(final Activity activity, final Facebook fb, SessionListener mSessionListener) {
    	init(activity, fb, new String[] {}, mSessionListener);
    }
    
    public void init(final Activity activity, final Facebook fb, final String[] permissions, SessionListener mSessionListener) {
        mActivity = activity;
        mFb = fb;
        mPermissions = permissions;
        mHandler = new Handler();
        
        setBackgroundColor(Color.TRANSPARENT);
        setAdjustViewBounds(true);
        setImageResource(fb.isSessionValid() ?
                         R.drawable.logout_button : 
                         R.drawable.login_button);
        drawableStateChanged();
        
        events.addAuthListener(mSessionListener);
        events.addLogoutListener(mSessionListener);
        setOnClickListener(this);
    }
    
    public void onClick(View arg0) {
    		this.click();
    }
    
    public void click(){
        if (mFb.isSessionValid()) {
        	events.onLogoutBegin();
            AsyncFacebookRunner asyncRunner = new AsyncFacebookRunner(mFb);
            asyncRunner.logout(getContext(), new LogoutRequestListener());
        } else {
            mFb.authorize(mActivity, mPermissions, new LoginDialogListener());
        }
    }
    
    public Handler getHandler(){
    	return this.mHandler;
    }

    private final class LoginDialogListener implements DialogListener {
        public void onComplete(Bundle values) {
        	events.onLoginSuccess();
        }

        public void onFacebookError(FacebookError error) {
        	events.onLoginError(error.getMessage());
        }
        
        public void onError(DialogError error) {
        	events.onLoginError(error.getMessage());
        }

        public void onCancel() {
        	events.onLoginError("Action Canceled");
        }
    }
    
    private class LogoutRequestListener extends BaseRequestListener {
        public void onComplete(String response) {
            // callback should be run in the original thread, 
            // not the background thread
            mHandler.postDelayed(new Runnable() {
                public void run() {
                	events.onLogoutFinish();
                }
            }, 300);
        }
    }
}
