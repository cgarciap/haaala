package com.facebook.android;

import com.facebook.android.SessionEvents.AuthListener;
import com.facebook.android.SessionEvents.LogoutListener;

public interface SessionListener extends AuthListener, LogoutListener {

}
