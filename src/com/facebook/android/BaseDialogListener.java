package com.facebook.android;

import android.util.Log;

import com.cgpcosmad.hala.log.HalaLog;
import com.facebook.android.Facebook.DialogListener;


/**
 * Skeleton base class for RequestListeners, providing default error 
 * handling. Applications should handle these error conditions.
 *
 */
public abstract class BaseDialogListener implements DialogListener {

    public void onFacebookError(FacebookError e) {
    	Log.e(HalaLog.TAG, "onFacebookError " + e.getMessage());
    }

    public void onError(DialogError e) {
    	Log.e(HalaLog.TAG, "onError " + e.getMessage());        
    }

    public void onCancel() {
    	Log.e(HalaLog.TAG, "onCancel ");   
    }
    
}
