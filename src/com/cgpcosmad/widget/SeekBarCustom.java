package com.cgpcosmad.widget;

import android.content.Context;

import android.util.AttributeSet;
import android.widget.*;
import android.widget.SeekBar.OnSeekBarChangeListener;

/**
 * Control personalizado que representa una barra de progreso con un título con:
 * @param min: 		Valor mánimo
 * @param progress: Valor actual
 * @param text:		Título mostrado
 * 
 * @author Carlos García. cgpcosmad@gmail.com
 */
public class SeekBarCustom extends LinearLayout implements OnSeekBarChangeListener {
	private static final String NS 		 = "http://schemas.android.com/apk/res/android";
	private static final String PROGRESS = "progress";
	private static final String MAX		 = "max";
	private static final String TEXT	 = "text";
	
	private int		 resCode;
	private SeekBar  bar;
	private TextView caption;
	
	public SeekBarCustom(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.initUI(attrs);
	}
	
	private void initUI(AttributeSet attrs){
		this.caption = new TextView(this.getContext());
		this.bar   	 = new SeekBar(this.getContext());
		this.resCode = attrs.getAttributeResourceValue(NS, TEXT, 0);
		
		this.setOrientation(LinearLayout.VERTICAL);
		this.addView(caption);
		this.addView(bar);

		int progress = attrs.getAttributeUnsignedIntValue(NS, PROGRESS, 1);
		caption.setText(getResources().getString(resCode, progress));
		
		bar.setMax(attrs.getAttributeUnsignedIntValue(NS, MAX, 100));
		bar.setProgress(progress);
		
		
		bar.setOnSeekBarChangeListener(this);
	}

	public void setProgress(int b){
		bar.setProgress(b);
	}
	
	public int getProgress(){
		return bar.getProgress();
	}
	
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
		if (progress <= 0){
			progress = 1;
		}
		caption.setText(getResources().getString(resCode, progress));
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {}
}
