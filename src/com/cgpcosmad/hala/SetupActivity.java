package com.cgpcosmad.hala;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TabActivity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.Toast;

import com.cgpcosmad.hala.background.IBackgroudTaskResultListener;
import com.cgpcosmad.hala.background.ValidatingConnectionToMailServerTask;
import com.cgpcosmad.hala.entities.Session;
import com.cgpcosmad.hala.entities.Setup;
import com.cgpcosmad.hala.utils.Utils;

/**
 * Configuración de la aplicación
 * @author Carlos García. cgpcosmad@gmail.com 
 */
public class SetupActivity  extends TabActivity implements IBackgroudTaskResultListener  {
    private static final int MY_DATA_CHECK_CODE = 1976;

	private Setup setup;
	private LinearLayout details;
	private Spinner	 providers;
	private CheckBox sslEnabled;
	private Button	 cmdEmail;
	private EditText login;
	private EditText password;
	private EditText port;
	private EditText host;

	private Button checkTTS;
	private Button installTTS;
	
	private ProgressDialog progressDialog;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        
        super.onCreate(savedInstanceState);
        
        TabHost tabHost = this.getTabHost();
        LayoutInflater.from(this).inflate(R.layout.setup, tabHost.getTabContentView(), true);
        
        
        String emailTitle = getResources().getString(R.string.emailShort);
        String ttsTitle   = getResources().getString(R.string.requirement);

        tabHost.addTab(tabHost.newTabSpec(emailTitle).setIndicator(emailTitle, getResources().getDrawable(R.drawable.email)).setContent(R.id.pref_email));
        tabHost.addTab(tabHost.newTabSpec(ttsTitle).setIndicator(ttsTitle, getResources().getDrawable(R.drawable.tts)).setContent(R.id.pref_tts));
        
        
        this.setup       =  Session.getInstance(this.getApplicationContext()).getSetup();
        this.providers = (Spinner)  this.findViewById(R.id.mail_servers);
        this.details  	 = (LinearLayout)  this.findViewById(R.id.details);
        this.host  		 = (EditText) this.findViewById(R.id.host);
        this.port     	 = (EditText) this.findViewById(R.id.port);
        this.login       = (EditText) this.findViewById(R.id.login);
        this.password    = (EditText) this.findViewById(R.id.password);
        this.cmdEmail	 = (Button) this.findViewById(R.id.cmdEmail);
        this.sslEnabled  = (CheckBox) this.findViewById(R.id.sslEnabled);
        this.checkTTS 	 = (Button) this.findViewById(R.id.checkTTS);
        this.installTTS	 = (Button) this.findViewById(R.id.installTTS);
		
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.mailServers, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        providers.setAdapter(adapter);
        
        
        
        
        this.providers.setSelection(setup.getProvider());
        this.host.setText(setup.getHost());
        this.port.setText(String.valueOf(setup.getPort()));
        this.login.setText(setup.getLogin());
        this.password.setText(setup.getPassword());
		this.sslEnabled.setChecked(setup.isSSLEnabled());   
       	
		
		
		this.providers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0,  View view, int pos, long id) {
				if (pos == 2){
					details.setVisibility(View.VISIBLE);
				} else {
					details.setVisibility(View.GONE);
				}
				
				
				// Gmail
				if (pos == 0){
					password.setVisibility(View.GONE);
				} else {
					password.setVisibility(View.VISIBLE);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {/* nada que hacer*/}
		});
		
		
		this.checkTTS.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					Intent checkIntent = new Intent();
					checkIntent
							.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
					startActivityForResult(checkIntent, MY_DATA_CHECK_CODE);
				} catch (ActivityNotFoundException ex) {
					Toast.makeText(SetupActivity.this, getResources().getString(R.string.ttsKO), Toast.LENGTH_LONG).show();
				}
			}
		});
		
		this.installTTS.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					startActivity(new Intent(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA));
				} catch (ActivityNotFoundException ex) {
					Toast.makeText(SetupActivity.this, getResources().getString(R.string.ttsKO), Toast.LENGTH_LONG).show();
				}
			}
		});
		
		
		this.cmdEmail.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showDialog(DIALOG_ACCOUNTS);
			}
		});
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.setup, menu);
        return true;
    }
    

	private void acceptClick() {
		Context   context   = this.getApplicationContext();
		Resources resources = context.getResources();
		int		  provider  = this.providers.getSelectedItemPosition();
		String	  email		= login.getText().toString();
		
		setup.setProvider(provider);
        setup.setLogin(email);
        setup.setPassword(password.getText().toString());

        
        if (! email.contains("@")){
        	Toast.makeText(SetupActivity.this, getResources().getString(R.string.invalidEmail), Toast.LENGTH_LONG).show();
        	return;
        }
		
		// Gmail
        if (provider == 0){
        	finish();
        	Session.getInstance(this).setEmailAccountValid(true);
			return;
		}
		
        
		if (provider == 0){		  // Gmail
			setup.setHost(resources.getString(R.string.mailHostGmail));
			setup.setSSLEnabled(Boolean.parseBoolean(resources.getString(R.string.mailUseSsl)));
			setup.setPort(Integer.parseInt(resources.getString(R.string.mailPort)));
		} else if (provider == 1){ // Hotmail
			setup.setHost(resources.getString(R.string.mailHostHotmail));
			setup.setSSLEnabled(Boolean.parseBoolean(resources.getString(R.string.mailUseSsl)));
			setup.setPort(Integer.parseInt(resources.getString(R.string.mailPort)));
		} else {
	        setup.setHost(host.getText().toString());
	       	setup.setPort(Integer.parseInt(port.getText().toString()));
	        setup.setSSLEnabled(sslEnabled.isChecked());			
		}
		
	
		
		
		ValidatingConnectionToMailServerTask talk = new ValidatingConnectionToMailServerTask(this, setup);
		talk.execute();
		
		// Validamos que los datos de conexión son correctos
    	progressDialog = new ProgressDialog(this);
    	progressDialog.setIcon(R.drawable.wait);
    	progressDialog.setTitle(R.string.connectingMailServer);
    	progressDialog.setMessage(getResources().getString(R.string.waitPlease));
    	progressDialog.setIndeterminate(true);
    	progressDialog.setCancelable(true);
    	progressDialog.show();	  
	}

    /* 
     * El usuario ha hecho clic en una de las opciones del menú
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	try {
	        if (R.id.ok == item.getItemId()) {
	        	this.acceptClick();
	        } else {
	        	finish();	
	        }
	    	return true;
    	} catch (Exception ex){
			String message = ex.getMessage();
			if (Utils.isEmpty(message)){
				message = ex.getClass().getName(); // Da un error por qué NullPointerException no tiene message
			}
    		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    		return false;
    	}
    } 	
    
	@Override
	public void backgroundTaskFinished(String code, Object result) {
		if (ValidatingConnectionToMailServerTask.CODE.equals(code)){
			Session	session = Session.getInstance(this.getApplicationContext());
			boolean resultB = (Boolean) result;
			session.setEmailAccountValid(resultB);		

			progressDialog.dismiss();
			
			if (resultB){
				this.finish();
			} else {
	            Toast.makeText(this, this.getResources().getString(R.string.connectingMailServerFail), Toast.LENGTH_LONG).show();
			}			
		}
	}  
	
    /*
     * @see android.app.Activity#onActivityResult(int, int, android.content.Intent)
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SetupActivity.MY_DATA_CHECK_CODE) {
        	int msg = R.string.ttsKO;
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
            	msg = R.string.ttsOK;
            }
            Toast.makeText(this, this.getResources().getString(msg), Toast.LENGTH_LONG).show();
        }
    }
    
	private static final int DIALOG_ACCOUNTS = 0;
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_ACCOUNTS:
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Select a Google account");
			final AccountManager manager = AccountManager.get(this);
			final Account[] accounts = manager.getAccountsByType("com.google");
			final int size = accounts.length;
			String[] names = new String[size];
			for (int i = 0; i < size; i++) {
				names[i] = accounts[i].name;
			}
			builder.setItems(names, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					login.setText(accounts[which].name);
				}
			});
			return builder.create();
		}
		return null;
	}
}