package com.cgpcosmad.hala;

import android.os.Bundle;

import com.facebook.android.DialogError;
import com.facebook.android.FacebookError;
import com.facebook.android.Facebook.DialogListener;

public class DummyDialogListener implements DialogListener {
	public void onFacebookError(FacebookError e) {}
	public void onError(DialogError e) {}
	public void onComplete(Bundle values) {}
	public void onCancel() {}
}
