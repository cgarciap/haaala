package com.cgpcosmad.hala.entities;

import java.util.HashMap;

import com.cgpcosmad.hala.utils.Utils;

import android.content.res.Resources;
import android.speech.tts.TextToSpeech;

public class Application extends DataReceived {
	private static final long serialVersionUID = -2838889529366838320L;
	public static final String TYPE = "a";
	
	public Application(String message) {
		super();
		long ctime = System.currentTimeMillis();
		this.setId(TYPE + ctime);
		this.setCtime(ctime);
		this.setBody(message);
	}

	@Override
	public void speak(TextToSpeech mTts, Resources resources) {
		HashMap<String, String> params = new HashMap<String, String>(); 
		params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, this.getId());
		
		String toSpeak		= Utils.prepareToSpeak(resources, this.getBody());		
		mTts.speak(toSpeak, TextToSpeech.QUEUE_ADD, params);
	}

	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public Attachment getAttachment() {
		return null;
	}
}
