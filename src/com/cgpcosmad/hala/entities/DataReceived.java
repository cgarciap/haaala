package com.cgpcosmad.hala.entities;

import java.io.Serializable;

import android.content.res.Resources;
import android.speech.tts.TextToSpeech;

public abstract class DataReceived implements Serializable {
	private static final long serialVersionUID = 4564619847630737426L;
	
	private String id;
	private String from;
	private String body;
	private long   ctime;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	
	public long getCtime() {
		return ctime;
	}
	public void setCtime(long ctime) {
		this.ctime = ctime;
	}	
	
	public abstract void speak(TextToSpeech mTts, Resources resources);
	public abstract String getType();
	public abstract Attachment getAttachment();
}
