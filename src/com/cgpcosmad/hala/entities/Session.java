package com.cgpcosmad.hala.entities;

import com.facebook.android.Facebook;
import android.content.Context;
import android.os.Handler;

/**
 * Los componentes de la app comparten memoria
 * @author Carlos García. cgpcosmad@gmail.com 
 */
public class Session {

	private static Session instance;
	
	private static final int TIMESTAMP_MARGIN = 5 * 60 * 1000;
    public  static final String APP_ID 		  = "178188428883869"; 
    public  static final String[] FACEBOOK_PERMISSIONS = {"read_mailbox", "read_stream", "friends_online_presence"};
    
	private Facebook facebook;
	private Setup	 setup;
	private Handler  svcHandler;
	private Handler  activityHandler;
	private long 	 tapSpeakEmailTimestamp; // Timestamp when user tap on speak email option.
	private long 	 tapSpeakFacebookTimestamp;// Timestamp when user tap on speak fb option.
	private boolean	 isServiceRunning;
	private boolean  emailAccountValid;
	private String   uid;
	private String	 friendUID;
	
	private Session(Context context){
		this.setSpeakEmailEnabled(false);
		this.setSpeakFacebookEnabled(false);
		
		this.isServiceRunning = false;
		this.setup 		      = Setup.getInstance(context);
		this.facebook         = new Facebook(APP_ID);
	}
	
	public void close(){
		this.setup.save();
		Session.instance = null;
	}
	
	public static Session getInstance(Context context){
		if (Session.instance == null){
			Session.instance = new Session(context);
		}
		return Session.instance;
	}
	
	public void setServiceHandler(Handler hwd){
		this.svcHandler = hwd;
	}
	
	
	public void setSpeakEmailEnabled(boolean enabled){
		if (enabled){
			tapSpeakEmailTimestamp = System.currentTimeMillis() - TIMESTAMP_MARGIN;
		} else {
			tapSpeakEmailTimestamp = Long.MAX_VALUE;
		}
	}
	

	public void setSpeakFacebookEnabled(boolean enabled) {
		if (enabled){
			tapSpeakFacebookTimestamp = System.currentTimeMillis() - TIMESTAMP_MARGIN;
		} else {
			tapSpeakFacebookTimestamp = Long.MAX_VALUE;
		}
	}		

	public boolean isSpeakEmailEnabled() {
		return (System.currentTimeMillis() > tapSpeakEmailTimestamp);
	}

	public boolean isSpeakFacebookEnabled() {
		return (System.currentTimeMillis() > tapSpeakFacebookTimestamp);
	}
	
	public long getTapSpeakEmailTimestamp() {
		return tapSpeakEmailTimestamp;
	}
	
	public long getTapSpeakFacebookTimestamp() {
		return tapSpeakFacebookTimestamp;
	}	
	
	public Handler getServiceHandler() {
		return svcHandler;
	}
	
	public Setup getSetup() {
		return setup;
	}

	public Facebook getFacebook() {
		return facebook;
	}

	
	public boolean isServiceRunning() {
		return isServiceRunning;
	}


	public void setServiceRunning(boolean isServiceRunning) {
		this.isServiceRunning = isServiceRunning;
	}

	public void setEmailAccountValid(boolean emailAccountValid) {
		this.emailAccountValid = emailAccountValid;
	}
	
	public boolean isEmailAccountValid() {
		return emailAccountValid;
	}

	public boolean isFacebookAccountValid() {
		return (this.facebook != null) && (facebook.isSessionValid());
	}
	
	public void setTapSpeakEmailTimestamp(long tapSpeakEmailTimestamp) {
		this.tapSpeakEmailTimestamp = tapSpeakEmailTimestamp;
	}

	public void setTapSpeakFacebookTimestamp(long tapSpeakFacebookTimestamp) {
		this.tapSpeakFacebookTimestamp = tapSpeakFacebookTimestamp;
	}

	public Handler getActivityHandler() {
		return activityHandler;
	}

	public void setActivityHandler(Handler activityHandler) {
		this.activityHandler = activityHandler;
	}


	public String getFriendUID() {
		return friendUID;
	}

	public void setFriendUID(String friendUID) {
		this.friendUID = friendUID;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}	
}