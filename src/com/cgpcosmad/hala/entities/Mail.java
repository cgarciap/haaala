package com.cgpcosmad.hala.entities;

import java.util.HashMap;

import com.cgpcosmad.hala.utils.Utils;

import com.cgpcosmad.hala.R;


import android.content.res.Resources;
import android.speech.tts.TextToSpeech;

public class Mail extends DataReceived {
	private static final long serialVersionUID = 2397885260356855135L;
	
	private String subject;
	public static final String TYPE = "m";
	
	public Mail(String id, String from, String subject, String body, long ctime) {
		super();
		this.setId(id);		
		this.setFrom(from);
		this.setBody(body);
		this.setCtime(ctime);
		this.subject = subject;
	}
	
	public String getSubject() {
		return subject;
	}

	@Override
	public void speak(TextToSpeech mTts, Resources resources) {
		StringBuilder buffer = new StringBuilder(2048);
		String		  NL	 = System.getProperty("line.separator");
		
		if (Utils.isEmpty(NL)){
			NL = "\n";
		}
		
		HashMap<String, String> params = new HashMap<String, String>(); 
		params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, this.getId()); 		
		
		buffer.append(resources.getString(R.string.emailFrom));
		buffer.append(this.getFrom());
		buffer.append(NL);
		buffer.append(resources.getString(R.string.emailSubject));
		buffer.append(this.getSubject());
		buffer.append(NL);		
		buffer.append(resources.getString(R.string.emailContent));
		buffer.append(this.getBody());
		
		
		String toSpeak	= Utils.prepareToSpeak(resources, buffer.toString());		
		mTts.speak(toSpeak, 	 TextToSpeech.QUEUE_ADD, params);
	}

	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public Attachment getAttachment() {
		return null;
	}	
		
}
