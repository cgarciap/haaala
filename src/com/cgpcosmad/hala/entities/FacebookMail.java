package com.cgpcosmad.hala.entities;

import java.util.HashMap;

import com.cgpcosmad.hala.utils.Utils;

import com.cgpcosmad.hala.R;

import android.content.res.Resources;
import android.speech.tts.TextToSpeech;

public class FacebookMail extends DataReceived {
	private static final long serialVersionUID = 6827922879457302490L;
	private String threadID;
	
	public FacebookMail(String threadID, String id, String from, String body, long ctime) {
		super();
		this.threadID = threadID;
		
		this.setId(id);	
		this.setFrom(from);
		this.setBody(body);
		this.setCtime(ctime);
	}
	
	@Override
	public void speak(TextToSpeech mTts, Resources resources) {
		StringBuilder buffer = new StringBuilder(2048);
		String		  NL	 = System.getProperty("line.separator");
		
		if (Utils.isEmpty(NL)){
			NL = "\n";
		}
		
		HashMap<String, String> params = new HashMap<String, String>(); 
		params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, this.getId()); 		
		
		buffer.append(resources.getString(R.string.fbMailFrom));
		buffer.append(this.getFrom());
		buffer.append(NL);
		buffer.append(resources.getString(R.string.fbMailBody));
		buffer.append(this.getBody());
		
		String toSpeak		= Utils.prepareToSpeak(resources, buffer.toString());		
		mTts.speak(toSpeak, 	 TextToSpeech.QUEUE_ADD, params);
	}
	
	@Override
	public String getType() {
		return "fbm";
	}	
	
	@Override
	public Attachment getAttachment() {
		return null;
	}

	public String getThreadId() {
		return threadID;
	}	
}
