package com.cgpcosmad.hala.entities;

public class Friend {
	private String uid;
	private String name;
	
	public Friend(String uid, String name) {
		super();
		this.uid = uid;
		this.name = name;
	}

	public String getUid() {
		return uid;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return name;
	}
}
