package com.cgpcosmad.hala.entities;

import java.io.Serializable;

import android.graphics.Bitmap;

public class Attachment implements Serializable {
	private static final long serialVersionUID = -7082729869045522221L;
	private String href;
	private String src;
	private String type;
	private Bitmap bitmap;

	public Attachment(String href, String src, String type) {
		super();
		this.href = href;
		this.src  = src;
		this.type = type;
	}
	
	public Attachment(String href, String src, String type, Bitmap bitmap) {
		this(href, src, type);
		this.bitmap = bitmap;
	}

	public String getHref() {
		return href;
	}
	
	public String getSrc() {
		return src;
	}	

	public String getType() {
		return type;
	}

	public Bitmap getBitmap() {
		return bitmap;
	}

	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}
}
