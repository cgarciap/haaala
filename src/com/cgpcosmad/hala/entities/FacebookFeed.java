package com.cgpcosmad.hala.entities;

import java.util.HashMap;

import com.cgpcosmad.hala.utils.Utils;


import com.cgpcosmad.hala.R;

import android.content.res.Resources;
import android.speech.tts.TextToSpeech;

public class FacebookFeed extends DataReceived {
	private static final long serialVersionUID = -5955984700258047579L;
	private boolean		comment;
	private Attachment	attachment;
	
	public FacebookFeed(String id, String from, String text, long ctime, boolean comment) {
		super();
		this.setId(id);
		this.setFrom(from);
		this.setBody(text);
		this.setCtime(ctime);
		this.comment = comment;
	}


	@Override
	public void speak(TextToSpeech mTts, Resources resources) {
		StringBuilder buffer = new StringBuilder(256);
		String		  NL	 = System.getProperty("line.separator");
		
		if (Utils.isEmpty(NL)){
			NL = "\n";
		}
		
		HashMap<String, String> params = new HashMap<String, String>(); 
		params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, this.getId());
		
/*
	 <string name="postPhoto">Facebook. Post a photo</string>
	 <string name="postVideo">Facebook. Post a video</string>
	 <string name="postLink">Facebook. Post a link</string>
	 <string name="postText">Facebook. Post a status</string>
	 
	 <string name="postCommentPhoto">Facebook. Comment on a photo</string>
	 <string name="postCommentVideo">Facebook. Comment on a video</string>
	 <string name="postCommentLink">Facebook. Comment on a link</string>	 
	 <string name="postCommentText">Facebook. Comment on status</string>	 		
 */
		
		// buffer.append(this.isComment() ? resources.getString(R.string.fbCommentFrom) : resources.getString(R.string.fbFeedFrom));
		// buffer.append(this.getFrom());

		if (this.attachment != null){
			String attachType = attachment.getType();
			int	   resID	  = 0;
			
			if (this.isComment()){
				resID = R.string.postCommentText;
				
				if ("photo".equals(attachType)){
					resID = R.string.postCommentPhoto;
				} else if ("video".equals(attachType)){
					resID = R.string.postCommentVideo;
				} else if ("link".equals(attachType)){
					resID = R.string.postCommentLink;
				}				
			} else {
				resID = R.string.postText;
				
				if ("photo".equals(attachType)){
					resID = R.string.postPhoto;
				} else if ("video".equals(attachType)){
					resID = R.string.postVideo;
				} else if ("link".equals(attachType)){
					resID = R.string.postLink;
				}
			}
			
			buffer.append(resources.getString(resID));
			buffer.append(resources.getString(R.string.from));
			buffer.append(this.getFrom());
		}

		
		
		if (Utils.isNotEmpty(this.getBody())){
			buffer.append(NL);
			buffer.append(this.isComment() ? resources.getString(R.string.fbCommentBody) : resources.getString(R.string.fbFeedBody));
			buffer.append(this.getBody());
		}
		
		String toSpeak	= Utils.prepareToSpeak(resources, buffer.toString());		
		
		mTts.speak(toSpeak, 	 TextToSpeech.QUEUE_ADD, params);
	}
	
	public boolean isComment() {
		return comment;
	}


	@Override
	public String getType() {
		if (this.isComment()){
			return "fbc";
		} else {
			return "fbf";
		}
	}
	
	public Attachment getAttachment() {
		return attachment;
	}


	public void setAttachment(Attachment attachment) {
		this.attachment = attachment;
	}

}
