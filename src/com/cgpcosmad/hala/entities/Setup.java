package com.cgpcosmad.hala.entities;

import com.cgpcosmad.hala.R;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;

/**
 * Configuración de la aplicación
 * @author Carlos García. cgpcosmad@gmail.com 
 */
public class Setup {
	private static Setup instance;
	
	
	private static final String FILE_KEY = "halatts";
	private static final String PROVIDER = "provider";
	private static final String HOST = "host";
	private static final String PORT = "port";	
	private static final String SSL = "ssl";		
	private static final String LOGIN = "login";
	private static final String PWD	  = "password";
	
	private static final String GENDER = "genger";
	
	private static final String TERM_OF_USE  = "termofuse";
	
	
	// Autenticacion de cara al servidor de correo electrónico
	private int		provider;
	private String  host; 	
	private int	    port;
	private boolean	sslEnabled;
	private String  login;
	private String  password;
	
	// Para anuncios más personalizados
	private String   adGender;
	
	// Terminos y condiciones de uso han sido aceptadas	
	private boolean termOfUseAccepted;
	
	
	private final Context context;
	
	public static Setup getInstance(Context context){
		if (Setup.instance == null){
			Setup.instance = new Setup(context);
		}
		return Setup.instance;
	}
	
	
	private Setup(Context context){
		this.context = context;
		
		Resources  		  resources	  = context.getResources();
        SharedPreferences preferences = context.getSharedPreferences(FILE_KEY, Context.MODE_PRIVATE);
        this.provider 	= preferences.getInt(PROVIDER, 0);	// El primero de la lista es gmail
        this.host	  	= preferences.getString(HOST, 	resources.getString(R.string.mailHostGmail));
        this.port	  	= preferences.getInt(PORT, 		Integer.valueOf(resources.getString(R.string.mailPort)));	
        this.sslEnabled = preferences.getBoolean(SSL, 	Boolean.parseBoolean(resources.getString(R.string.mailUseSsl)));
		this.login		= preferences.getString(LOGIN,  "");
		this.password	= preferences.getString(PWD, 	"");
		
		this.adGender	= preferences.getString(GENDER,	   "");
		this.termOfUseAccepted = preferences.getBoolean(TERM_OF_USE, false);
	}
    
    
    public boolean save() {
    	SharedPreferences preferences = context.getSharedPreferences(FILE_KEY, Context.MODE_PRIVATE);
        Editor			  editor	  = preferences.edit();
        
        editor.putInt(PROVIDER,  provider);
        editor.putString(HOST,  host);
        editor.putInt(PORT,  port); 
        editor.putBoolean(SSL,  sslEnabled);              
        editor.putString(LOGIN,  login);
        editor.putString(PWD, 	 password);
        editor.putString(GENDER,	 adGender);
        editor.putBoolean(TERM_OF_USE,  termOfUseAccepted);         
        
        return editor.commit();
    }
    
    public void clear() {
        Editor editor = context.getSharedPreferences(FILE_KEY, Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.commit();
    }
	
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public boolean isSSLEnabled() {
		return this.sslEnabled;
	}

	public void setSSLEnabled(boolean b) {
		this.sslEnabled = b;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public int getProvider() {
		return provider;
	}

	public void setProvider(int provider) {
		this.provider = provider;
	}	
	
	public long getIntervalInMilliseconds(){
		return 20000; //2 * 60 * 1000;
	}


	public String getAdGender() {
		return adGender;
	}


	public void setAdGender(String adGender) {
		this.adGender = adGender;
	}

	public boolean isTermOfUseAccepted() {
		return termOfUseAccepted;
	}
	
	public void setTermOfUseAccepted(boolean termOfUseAccepted) {
		this.termOfUseAccepted = termOfUseAccepted;
	}

}
