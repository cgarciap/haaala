package com.cgpcosmad.hala.background;

public interface IBackgroudTaskResultListener {
	/**
	 * @param code 		Código de la operación que ha finalizado
	 * @param result	Resultado de la operación
	 */
	public void backgroundTaskFinished(String code, Object result);
}
