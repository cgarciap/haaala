package com.cgpcosmad.hala.background;

import javax.mail.MessagingException;
import javax.mail.Store;

import com.cgpcosmad.hala.entities.Setup;
import com.cgpcosmad.hala.utils.InboxEmailUtils;


import android.os.AsyncTask;

/**
 * Valida la conexión con el servidor de correo 
 * @author Carlos García. cgpcosmad@gmail.com 
 */
public class ValidatingConnectionToMailServerTask extends AsyncTask<Void, Void, Boolean> {  // <Params, Progress, Result> 
	public static final String CODE = "checkMailServerConnection";
	
	private IBackgroudTaskResultListener listener;
	private Setup	 setup;
	
	public ValidatingConnectionToMailServerTask(IBackgroudTaskResultListener activity, Setup setup){
		this.listener  = activity;
		this.setup	   = setup;
	}
	

	protected Boolean doInBackground(Void... params) {
		boolean	isOk = true;
		
		
		if (setup.getProvider() == 0){ // Gmail
			isOk = true;
		} else {
			Store	store = null;
		
			try {
				store = InboxEmailUtils.connect(setup);
			} catch (MessagingException e) {
				isOk = false;
			} finally {
				try {
					if (store != null){store.close();}
				} catch (Exception e) {}
			}
		}
		return isOk;
	}
	
	@Override
	protected void onPostExecute(Boolean result) {
		listener.backgroundTaskFinished(CODE, result);
	}
}

