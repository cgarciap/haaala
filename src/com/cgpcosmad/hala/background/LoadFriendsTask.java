package com.cgpcosmad.hala.background;

import java.util.ArrayList;
import android.os.AsyncTask;

import com.cgpcosmad.hala.entities.Friend;
import com.cgpcosmad.hala.utils.FacebookUtils;
import com.facebook.android.Facebook;

/**
 * Carga los contactos de Facebook
 * @author Carlos García. cgpcosmad@gmail.com 
 */
public class LoadFriendsTask extends AsyncTask<Void, Void, ArrayList<Friend>> {  // <Params, Progress, Result> 
	public static final String CODE = "loadFriends";
	
	private IBackgroudTaskResultListener listener;
	private Facebook 	 fbAPI;
	
	public LoadFriendsTask(IBackgroudTaskResultListener activity, Facebook fbAPI){
		this.listener  = activity;
		this.fbAPI	   = fbAPI;
	}
	
	protected ArrayList<Friend> doInBackground(Void... params) {
		return FacebookUtils.getFriends(fbAPI);
	}
	
	@Override
	protected void onPostExecute(ArrayList<Friend> friends) {
		listener.backgroundTaskFinished(CODE, friends);
	}
}

