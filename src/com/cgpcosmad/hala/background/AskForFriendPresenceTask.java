package com.cgpcosmad.hala.background;

import java.util.ArrayList;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import com.cgpcosmad.hala.R;
import com.cgpcosmad.hala.utils.FacebookUtils;
import com.cgpcosmad.hala.utils.Utils;
import com.facebook.android.Facebook;

/**
 * Muestra los contactos conectados en el chat
 * @author Carlos García. cgpcosmad@gmail.com 
 */
public class AskForFriendPresenceTask extends AsyncTask<Void, Void, ArrayList<String>> {  // <Params, Progress, Result> 
	private Activity activity;
	private Facebook fbAPI;
	
	public AskForFriendPresenceTask(Activity activity, Facebook fbAPI){
		this.activity  = activity;
		this.fbAPI	   = fbAPI;
	}
	
	protected ArrayList<String> doInBackground(Void... params) {
		return FacebookUtils.getOnChatFriends(fbAPI);
	}
	
	@Override
	protected void onPostExecute(ArrayList<String> onChatFriends) {
		StringBuilder builder = new StringBuilder(255);
		String		  NL	  = System.getProperty("line.separator");
		
		if (Utils.isEmpty(NL)){
			NL = "\n";
		}
		
		if (Utils.isNotEmpty(onChatFriends)){
			builder.append(activity.getResources().getString(R.string.chatHasPresence));
			builder.append(NL);
			builder.append(NL);
			
			for (String friendName : onChatFriends){
				builder.append("- ");
				builder.append(friendName);
				builder.append(NL);			
			}
		} else {
			builder.append(activity.getResources().getString(R.string.chatNobody));
		}
		
		Toast.makeText(activity.getApplicationContext(), builder.toString(), Toast.LENGTH_LONG).show();
	}
}

