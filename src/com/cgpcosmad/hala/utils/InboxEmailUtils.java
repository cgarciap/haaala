package com.cgpcosmad.hala.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.BodyPart;
import javax.mail.FetchProfile;
import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMultipart;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.cgpcosmad.hala.dao.Database;
import com.cgpcosmad.hala.entities.Mail;
import com.cgpcosmad.hala.entities.Setup;
import com.cgpcosmad.hala.log.HalaLog;

/**
 * Utilidades para el tratamiento de correos electrónicos
 * @author Carlos García. cgpcosmad@gmail.com 
 */
public class InboxEmailUtils {
	private static final String URI_PREFIX = "content://gmail-ls/conversations/";
	private static final String[] CONVERSATION_PROYECTION = {"_id", "date"};
	private static final String[] MESSAGE_PROJECTION		= {"_id", "subject", "fromAddress", "body", "dateReceivedMs"};
	
	
	public static Store connect(Setup setup)  throws MessagingException {
		if (Utils.isEmpty(setup.getLogin()) || Utils.isEmpty(setup.getPassword())){
			throw new MessagingException();
		}
		
	    Properties props  = new Properties();
	    props.setProperty("mail.pop3.connectiontimeout", "20000");	// 20 segundos
	    props.setProperty("mail.pop3.timeout",	  		 "30000");	// 30 segundos
	    props.setProperty("mail.pop3.starttls.enable",	  "false");
	    props.setProperty("mail.pop3.port", 			  String.valueOf(setup.getPort()));
	    props.setProperty("mail.pop3.socketFactory.port", String.valueOf(setup.getPort()));
	    
	    
	    // Hay que usar SSL
	    if (setup.isSSLEnabled()){
	    	props.setProperty("mail.pop3.socketFactory.class",	  "javax.net.ssl.SSLSocketFactory" );
	    	props.setProperty("mail.pop3.socketFactory.fallback", "false");
	    }
	    
	    Session session = Session.getDefaultInstance(props);
	    Store	store   = session.getStore("pop3");	    
	    String  login   = setup.getLogin();
	    
	    if (login.contains("@gmail")){
	    	login = "recent:" + login;
	    }
	    
	    store.connect(setup.getHost(), login, setup.getPassword());
	    
	    return store;
	}

	
	public static ArrayList<Mail> getUnreadedGmailEmails(Context context, Date fromTime, Database database, Setup setup) throws MessagingException, IOException {
		ArrayList<Mail> mails  = new ArrayList<Mail>(32);
        String	 		email  = setup.getLogin();
        Cursor			cCursor	= null;
        boolean			finish = false;
        Date			mDate = null;
        
        try {
			ContentResolver resolver = context.getContentResolver();
			
	        // Conversaciones ordenadas por fecha (de más reciente a más antigua)
	        cCursor	= resolver.query(Uri.parse(URI_PREFIX + email), CONVERSATION_PROYECTION, "label:^i" , null, null);	
	        while (cCursor.moveToNext() && (! finish)) {
	        	mDate  = new Date(cCursor.getLong(cCursor.getColumnIndex("date")));
	        	finish = mDate.before(fromTime);
        		
            	if (! finish){
	        		// Obtenemos un cursor para esa conversación.
	        		String conv_id  = cCursor.getString(cCursor.getColumnIndex("_id"));
	        		Uri	   uri	    = Uri.parse(URI_PREFIX + email + "/" + conv_id + "/messages");
	                Cursor mCursor	= resolver.query(uri, MESSAGE_PROJECTION, null, null, null);
	                
	                while (mCursor.moveToNext() && (! finish)) {
	                	long mtime = mCursor.getLong(mCursor.getColumnIndex("dateReceivedMs"));
    	    			mDate      = new Date(mtime);
                    	finish     = mDate.before(fromTime);
                    	
                    	if (! finish){
                    		String id = mCursor.getString(mCursor.getColumnIndex("_id"));
                			if (database.contains(id)){
                				continue;
                			}
                			
        	    			String from		= mCursor.getString(mCursor.getColumnIndex("fromAddress"));
        	    			// Not speak facebook messages
        	    			if ((from.contains(email)) || (from.toLowerCase().contains("facebook"))){
            					continue;
            				}
        	    			
        	    			String subject	= mCursor.getString(mCursor.getColumnIndex("subject"));
        	    			String body 	= mCursor.getString(mCursor.getColumnIndex("body"));
        	    			
            				body = Utils.abbreviate(body, 2048);
            				
                    		mails.add(new Mail(id, getNameFromAddressString(from), subject, body, mtime));                        		
                    	}
	                }
	                Utils.closeQuietly(mCursor);
                }
	    	}
		} catch (Exception ex){	
			Log.e(HalaLog.TAG, ex.toString());
		} finally {
			Utils.closeQuietly(cCursor);
		}
		
		return mails;
	}
	
	public static ArrayList<Mail> getUnreadedEmails(Context context, long fromTime, Database database, Setup setup) throws MessagingException, IOException{
		if (setup.getProvider() == 0){	// Gmail
			return getUnreadedGmailEmails(context, new Date(fromTime), database,  setup);
		}
		
		ArrayList<Mail> mails = new  ArrayList<Mail>();
	    Folder inbox = null;
	    Store  store = null;
		
		
		try {
			store = connect(setup);
		    inbox = store.getFolder("INBOX");
		    if (! inbox.isOpen()){
		    	inbox.open(Folder.READ_ONLY);
		    }
		    
		    javax.mail.Message[] messages = inbox.getMessages();
		    
		    
		    FetchProfile fp = new FetchProfile();
		    fp.add(FetchProfile.Item.FLAGS);
		    fp.add(FetchProfile.Item.CONTENT_INFO);
		    fp.add("Date");
		    inbox.fetch(messages, fp);
		    
		    if ((messages == null) || (messages.length == 0)){
		    	 messages = inbox.getMessages();
		    }

		    Date cutDate = new Date(fromTime);
		    for (int i = messages.length - 1; i >= 0; i--) {
		    	Date	mailDate = messages[i].getSentDate();
    			String	id	  	 = "m" + mailDate.getTime();
    			
    			if (database.contains(id)){
    				continue;
    			}
    			
	    		if (mailDate.getTime() > cutDate.getTime()){
	    			if (messages[i].getSize() > (1024 * 64)){
	    				continue;
	    			}
	    			
	    			String from		= "";
	    			String subject	= messages[i].getSubject();
	    			String body 	= null;
	    			
	    			try {
	    				InternetAddress addr = (InternetAddress) messages[i].getFrom()[0];
	    				// Not speak facebook messages
		    			if (addr.getAddress().contains("facebook")){
	    					continue;
	    				}	    				
	    				from = addr.getPersonal();
	    				if (Utils.isEmpty(from)){
	    					from =  addr.getAddress();
	    				}
	    			} catch (Exception ex){}
    				
	    			// Not speak facebook messages
	    			if (from.toLowerCase().contains("facebook")){
    					continue;
    				}
    				
	    			Object content = messages[i].getContent();
	    			if (content instanceof MimeMultipart){
	    				MimeMultipart multiMime = (MimeMultipart) content;
	    				BodyPart	  bodypart	= multiMime.getBodyPart(0);
	    				body = bodypart.getContent().toString();
	    			} else if (content instanceof BodyPart){
	    				BodyPart bodypart = (BodyPart) content;
	    				body 			  = bodypart.getContent().toString();
	    			} else {
	    				body = messages[i].getContent().toString();
	    			}
	    			
	    			if (body.contains("javax.mail")){
	    				continue;
	    			}
    				body = Utils.abbreviate(body, 2048);
    				mails.add(new Mail(id, from, subject, body, mailDate.getTime()));
	    		} else {
	    			break;
	    		}
		    }
		} finally {
			try {
				store.close();
			} catch (Exception e) {}
			
			try {
				if (inbox.isOpen()){
					inbox.close(false);
				}
			} catch (Exception e) {}
			
		}		
		
		return mails;
	}
	
	
    private static final Pattern NAME_ADDRESS_PATTERN = Pattern.compile("\"(.*)\"");
    private static final Pattern UNNAMED_ADDRESS_PATTERN = Pattern.compile("([^<]+)@");
    public static String getNameFromAddressString(String addressString) {
        Matcher namedAddressMatch = NAME_ADDRESS_PATTERN.matcher(addressString);
        if (namedAddressMatch.find()) {
            String name = namedAddressMatch.group(1);
            if (name.length() > 0) return name;
            addressString =
                    addressString.substring(namedAddressMatch.end(), addressString.length());
        }

        Matcher unnamedAddressMatch = UNNAMED_ADDRESS_PATTERN.matcher(addressString);
        if (unnamedAddressMatch.find()) {
            return unnamedAddressMatch.group(1);
        }

        return addressString;
    }
    
}
