
package com.cgpcosmad.hala.utils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;

import com.cgpcosmad.hala.dao.Database;
import com.cgpcosmad.hala.entities.Attachment;
import com.cgpcosmad.hala.entities.FacebookFeed;
import com.cgpcosmad.hala.entities.FacebookMail;
import com.cgpcosmad.hala.entities.Friend;
import com.cgpcosmad.hala.entities.Session;
import com.cgpcosmad.hala.log.HalaLog;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.android.Util;


/**
 * Utilidades para el tratamiento de facebook
 * @author Carlos García. cgpcosmad@gmail.com 
 */
public class FacebookUtils {
	private static HashMap<String, String> namesCache = new HashMap<String,String>(32);	// uid, name
	
	private static String executeFQL(Facebook fbAPI, String fql)  throws IOException, JSONException, FacebookError {
	    Bundle params = new Bundle();
        params.putString("format", "json");
        params.putString("access_token", fbAPI.getAccessToken());
        params.putString("query", fql);
        
		String json = Util.openUrl("https://api.facebook.com/method/fql.query", "GET", params);
		
		return json;
	}
	
	public static String getPublicProperty(Context context, String uid, String property)  throws IOException, JSONException, FacebookError {
	    String value = null;
	    
	    
	    if ("name".equals(property) && FacebookUtils.namesCache.containsKey(uid)){
	    	value = FacebookUtils.namesCache.get(uid);
	    } else{
			try {
		    	Facebook	fbAPI = Session.getInstance(context).getFacebook();
		        String		resp  = fbAPI.request(uid);
		        JSONObject	json  = Util.parseJson(resp);
		        
		        value = json.getString(property);
		        
		        // Si hemos obtenido un valor lo cacheamos.
		        if ((value != null) && "name".equals(property)){
		        	FacebookUtils.namesCache.put(uid, value);
		        }
		        
		    } catch (NullPointerException e) {
		    	Log.e(HalaLog.TAG, e.getMessage());
			} catch (org.json.JSONException e){
				// La propiedad solicitada no está en el resultado 
			}
	    }

	    return value;
	}
	
	private static Attachment getAttachment(JSONObject msg) throws JSONException {
		Attachment attach = null;
		
		if (msg.has("attachment")){
			JSONObject attachment = msg.getJSONObject("attachment");
			String	   type  	  = "link";
			String	   href		  = Utils.getJSONProperty(attachment, "href");
			String	   src		  = Utils.getJSONProperty(attachment, "src");
			
			if (attachment.has("media")){
				JSONArray amedia = attachment.getJSONArray("media");
				if (amedia.length() > 0){
					JSONObject media = amedia.getJSONObject(0);
					type  = Utils.getJSONProperty(media, "type");
					src   = Utils.getJSONProperty(media, "src");
					href  = Utils.getJSONProperty(media, "href");
				}
			}
			
			if (Utils.isNotEmpty(href) && Utils.isNotEmpty(type)){
				Bitmap bitmap = null;
				try {
					for (int i = 0; (i < 3) && (bitmap == null); i++){ // Como a veces falla, lo intentamos 3 veces
						bitmap = Utils.loadBitmap(src);
					}
				} catch (Exception ex){
					Log.e(HalaLog.TAG, ex.getMessage());
				}
				attach = new Attachment(href, src, type, bitmap);
			}
		}
		
		return attach;
	}
	
	/**
	 * @param context
	 * @param fromTime Sólo devolverá los mensajes de facebook recibidos desde ese instante de tiempo
	 * @return Devuelve los mensajes de facebook no leidos
	 * @throws JSONException 
	 * @throws FacebookError 
	 */
	public static ArrayList<FacebookFeed> getFeeds(Context context, String uid, long fromTime, Database database)  throws IOException, JSONException, FacebookError {
		ArrayList<FacebookFeed> feeds  = new ArrayList<FacebookFeed>();
	    Facebook 				fbAPI  = Session.getInstance(context).getFacebook();
	    
	    
//	    Log.i(HalaLog.TAG, fbAPI.getAccessToken());
	    
// 		La primera es la correcta	    
	    String 					resp   = executeFQL(fbAPI, "SELECT post_id, actor_id, created_time, updated_time, message, comments, attachment FROM stream WHERE (source_id=" + uid + ") AND (is_hidden = 0) AND updated_time >=" + (fromTime / 1000) + " order by updated_time asc limit 50");
//	    String 					resp   = executeFQL(fbAPI, "SELECT post_id, actor_id, created_time, updated_time, message, comments, attachment FROM stream  WHERE source_id in (SELECT target_id FROM connection WHERE source_id=me() AND is_following=1) AND is_hidden = 0) AND updated_time >=" + (fromTime / 1000) + "   order by updated_time asc limit 50");
//	    String 					resp   = executeFQL(fbAPI, "SELECT post_id, actor_id, created_time, updated_time, message, comments, attachment FROM stream  WHERE source_id in (SELECT target_id FROM connection WHERE source_id=" + uid + ") AND is_hidden = 0 AND updated_time >=" + (fromTime / 1000) + "   order by updated_time asc limit 50");
	    
		if (Utils.isNotEmpty(resp) && (resp.startsWith("["))){
			JSONArray	 msgs	= new JSONArray(resp);
			Attachment	 attach = null;
			FacebookFeed fbf    = null;
			
			for (int i = msgs.length() - 1; i >= 0; i--){
				attach = null;
				
				try {
					JSONObject msg   = msgs.getJSONObject(i);
					String id		 = "fbf"+ msg.getString("post_id");
					String author_id = msg.getString("actor_id");
					String body   	 = msg.getString("message");
					long   ctime	 = msg.getLong("created_time") * 1000; // Nos la da en segundos
					long   utime	 = msg.getLong("updated_time") * 1000; // Nos la da en segundos
					
					
					if (ctime >= fromTime){
						if (! database.contains(id)){
							String from = FacebookUtils.getPublicProperty(context, author_id, "name");
		    				fbf    = new FacebookFeed(id, from, body, ctime, false);
		    				attach = FacebookUtils.getAttachment(msg);
							
		    				if (attach != null){
		    					fbf.setAttachment(attach);
		    				}
							feeds.add(fbf);
							
						}
					} 
					
					if (ctime != utime) {
						// Alguno de los comentarios del post 
						JSONObject comments   = msg.getJSONObject("comments");
						
						if (comments != null){
							JSONArray commentList = comments.getJSONArray("comment_list");
							if (commentList != null){
								for (int j = 0; j < commentList.length(); j++){
									try {
										JSONObject comment   	= commentList.getJSONObject(j);
										long   	   comment_date = comment.getLong("time") * 1000; // Nos la da en segundos
		
										if (comment_date >= fromTime){
											String comment_id = "fbc" + comment.getString("id");
											
											if (database.contains(comment_id)){
												continue;
											}
											
											String comment_body  	   = comment.getString("text");
											String comment_author_id   = comment.getString("fromid");
											String comment_authod_name = FacebookUtils.getPublicProperty(context, comment_author_id, "name");
						    				
											fbf = new FacebookFeed(comment_id, comment_authod_name, comment_body, comment_date, true);
											
											if (attach == null){
												attach = FacebookUtils.getAttachment(msg);
											}
											
											if (attach != null){
												fbf.setAttachment(attach);
											}
											
											feeds.add(fbf);
											
										}
									} catch (Exception e) {
										Log.e(HalaLog.TAG, e.getMessage());
									}
								}
							}
						}
					}
				} catch (Exception e) {
					Log.e(HalaLog.TAG, e.getMessage());
				}
			}
		}	
			
	    return feeds;
	}
	
	public static JSONObject getBirthdayAndSex(Context context)  {
		JSONObject json	  = null;
		
		try {
			Facebook   fbAPI  = Session.getInstance(context).getFacebook();
			String 	   resp   = executeFQL(fbAPI, "select sex from user where uid=me()");
			
			if (Utils.isNotEmpty(resp)){
				JSONArray json_array   = new JSONArray(resp);
				json =  json_array.getJSONObject(0);
			}
		} catch (java.lang.Throwable ex){ //  IOException, JSONException, FacebookError
			return null;
		}
		
		return json;
	}
	
	/**
	 * @param fromTime Sólo devolverá los mensajes de facebook recibidos desde ese instante de tiempo
	 * @return Devuelve los mensajes de facebook recibidos desde fromTime
	 */
	public static ArrayList<FacebookMail> getInbox(Context context, long fromTime, Database database)  throws IOException, JSONException, FacebookError {
		ArrayList<FacebookMail> feeds  = new ArrayList<FacebookMail>();
		Facebook 				fbAPI  = Session.getInstance(context).getFacebook();
		String 					resp   = executeFQL(fbAPI, "SELECT message_id, author_id, body, created_time, thread_id FROM message WHERE (author_id <> me()) AND created_time>= " + (fromTime/1000) + " AND thread_id IN (SELECT thread_id FROM thread WHERE folder_id=0) order by created_time asc limit 50");
		
		
		final String NL = Utils.getLineSeparator();
		
		if (Utils.isNotEmpty(resp) && (resp.startsWith("["))){
			JSONArray msgs = new JSONArray(resp);
			
			for (int i = 0, num = msgs.length(); i < num; i++){
				try {
					JSONObject msg   = msgs.getJSONObject(i);
					String id		 = "fbm" + msg.getString("message_id");
					
					if (database.contains(id)){
						continue;
					}					
					
					String author_id = msg.getString("author_id");
					String body   	 = msg.getString("body");
					long   date		 = msg.getLong("created_time") * 1000; // Nos la da en segundos
					String thread_id = msg.getString("thread_id");
					
					
					if (date >= fromTime){
						String from = FacebookUtils.getPublicProperty(context, author_id, "name");
						
						FacebookMail mail = findEmailByThreadID(feeds, thread_id);
						if (mail == null){
							mail = new FacebookMail(thread_id, id, from, body, date);

							feeds.add(mail);
						} else {
							mail.setBody(mail.getBody() + NL + body);	
							
							// a) No tengo que agregarlo a feeds por que ya tengo la referencia, sencillamente actualizo el contenido de la misma.
							
							// b) En database sólo se gragará el primer ID
							database.add(new FacebookMail(thread_id, id, from, body, date));
						}
					}
				} catch (NullPointerException e) {
					Log.d(HalaLog.TAG, e.getLocalizedMessage());
				}
			}
		}

		return feeds;
	}
	
	private static FacebookMail findEmailByThreadID(List<FacebookMail> feeds, String thread_id) {
		for (FacebookMail feed  : feeds){
			if (thread_id.equals(feed.getThreadId())){
				return feed;
			}
		}
		return null;
	}

	public static ArrayList<Friend> getFriendsAux(Facebook fbAPI, String fql)  throws Exception, FacebookError {
		ArrayList<Friend> friends = new ArrayList<Friend>(128);
		String   resp  = executeFQL(fbAPI, fql);
		
		if (Utils.isNotEmpty(resp) && (resp.startsWith("["))){
			JSONArray msgs  = new JSONArray(resp);
			JSONObject msg  = null;
			String	   uid  = null;
			String	   name = null;
			
			for (int i = 0, num = msgs.length(); i < num; i++){
				msg  = msgs.getJSONObject(i);
				uid  = msg.getString("uid");
				name = msg.getString("name");
				
				// name = Utils.abbreviate(name, 18);
				
				friends.add(new Friend(uid, name));
			}
		}
		
		return friends;
	}
	
	public static ArrayList<Friend> getFriends(Facebook fbAPI)  {
		ArrayList<Friend> friends = new ArrayList<Friend>(128);
		
		try {
			ArrayList<Friend> friends1 = getFriendsAux(fbAPI, "SELECT uid, name FROM user WHERE uid = me()");
			ArrayList<Friend> friends2 = getFriendsAux(fbAPI, "SELECT uid, name FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1 = me()) order by name asc");
			
			friends.addAll(friends1);
			friends.addAll(friends2);
			
		} catch (Exception ex) {
			// En caso de error devuelve lista vacia
		} catch (FacebookError ex){
			// En caso de error devuelve lista vacia
		}
	 
	 
	 return friends;
		 
	}	
	
	
	public static ArrayList<String> getOnChatFriends(Facebook fbAPI){
		ArrayList<String> friends = new ArrayList<String>(32);
		
		try {
			String resp  = executeFQL(fbAPI, "SELECT name FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1 = me()) AND online_presence='active' order by name asc");
			
			if (Utils.isNotEmpty(resp) && (resp.startsWith("["))){
				JSONArray  msgs = new JSONArray(resp);
				JSONObject msg  = null;
				String	   name = null;
				
				for (int i = 0, num = msgs.length(); i < num; i++){
					msg  = msgs.getJSONObject(i);
					name = msg.getString("name");
					friends.add(name);
				}
			}
		} catch (Exception ex) {
			// En caso de error devuelve lista vacia
		} catch (FacebookError ex){
			// En caso de error devuelve lista vacia
		}
		
		return friends;
	}
	
}
