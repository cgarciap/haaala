package com.cgpcosmad.hala.utils;

import java.io.InputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import com.cgpcosmad.hala.R;
import com.cgpcosmad.hala.log.HalaLog;

/**
 * Clase de utilidad genérica
 * @author Carlos García. cgpcosmad@gmail.com
 */
public final class Utils {
	private static final String SERVICE_PACKAGE = "com.cgpcosmad.hala";
	
	public static InetAddress getInetAddress() {
		InetAddress ret = null;
		
		try {
	    	Enumeration<NetworkInterface> netInterfaces = NetworkInterface.getNetworkInterfaces();
	    	
	        while (netInterfaces.hasMoreElements()) {
	        	NetworkInterface netInterface		 = netInterfaces.nextElement();
	        	Enumeration<InetAddress> ips  		 = netInterface.getInetAddresses();
	            try {
	            	InetAddress inetAddress = ips.nextElement();
	            	if (! inetAddress.isLoopbackAddress()){
	            		ret = inetAddress;
					}
				} catch (Exception e) {
					// Nada, probamos con la siguiente dirección
				}
	        }
	    } catch (SocketException ex) {
	    	// Descartamos
	    	return null;
	    }
    	return ret;
	}
	
	public static boolean isEmpty(String str) {
		return ((str == null) || ("".equals(str.trim())));
	}
	
	public static boolean isNotEmpty(String str) {
		return ! Utils.isEmpty(str);
	}
	
	
	
    /**
     * Inicia el servicio 
     * @param context
     * @return Indica si se ha detenido correctamente
     */
    public static boolean startService(Context context, String serviceClassName){
		ComponentName comp   = new ComponentName(SERVICE_PACKAGE, serviceClassName);
		Intent 		  intent = new Intent().setComponent(comp);
		ComponentName name 	 = context.startService(intent);
		boolean isRunning	 = (name != null);
    	
		if (! isRunning){
			Log.e(HalaLog.TAG, "NOOOOOOOOOOOOOOOOOOOOOOOOO isServiceRunning ");
		}
		return isRunning;
    }	

    /**
     * Detenemos el servicio. 
     * @param context
     * @return Indica si se ha detenido correctamente
     */
    public static boolean stopService(Context context, String serviceClassName){
		ComponentName comp  	= new ComponentName(SERVICE_PACKAGE, serviceClassName);
		Intent		  intent    = new Intent().setComponent(comp);
		boolean 	  isOk	    = true;
		
		if (Utils.isServiceRunning(context, serviceClassName)){
			isOk = context.stopService(intent);
		}
		
		return isOk;
    }
    
	public static void cancelAlarm(Context context) {
		Intent		  intent  = new Intent("com.cgpcosmad.hala.intentALARM_MANAGER");
		PendingIntent pIntent = PendingIntent.getBroadcast(context, 0 , intent, 0);
		AlarmManager  manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		manager.cancel(pIntent);
	}
	
	public static void createRepeatingAlarm(Context context, long triggerAtTime, long interval){
		Intent		  intent  = new Intent("com.cgpcosmad.hala.intentALARM_MANAGER");
		PendingIntent pIntent = PendingIntent.getBroadcast(context, 0 , intent, 0);
		AlarmManager  manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		manager.setRepeating(AlarmManager.RTC_WAKEUP, triggerAtTime, interval, pIntent);
	}

    public static void showMessageDialog(Context context, int resourceId){
    	CharSequence message = context.getResources().getText(resourceId);
    	Utils.showMessageDialog(context, message.toString());
    }    
    
    public static void showMessageDialog(Context context, String message){
     	AlertDialog.Builder builder = new AlertDialog.Builder(context);
    	builder.setTitle(message);
    	builder.setPositiveButton(R.string.ok, null);
    	builder.create().show();
    }    
    
    public static boolean isServiceRunning(Context context, String fullClassServiceSearched){
    	ActivityManager 		 manager   = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
    	List<RunningServiceInfo> services  = manager.getRunningServices(Integer.MAX_VALUE);
    	boolean 				 isRunning = false;
    	
    	for (int i = 0, num = services.size(); i < num && (! isRunning); i++){
    		RunningServiceInfo info  = services.get(i);
			if (fullClassServiceSearched.equals(info.service.getClassName())){
				 isRunning = info.started;
			}
    	}
    	return isRunning;
    }

	public static boolean isNotEmpty(ArrayList<?> data) {
		return ((data != null) && (! data.isEmpty()));
	}

	public static String abbreviate(String body, int numChars) {
		StringBuilder toRet = new StringBuilder(numChars + 3);
		
		try {
			 toRet.append(body.substring(0, numChars));
			 toRet.append("...");
			 
		} catch (Exception ex){
			toRet.append(body);
		}
		
		return toRet.toString();
	}
	
	
	public static String prepareToSpeak(Resources res, String message) {
		String prepared  = message;
		int[]  	  smilesCode = {R.string.happySmiley, R.string.happySmiley,  R.string.sadSmiley,    R.string.sadSmiley,    R.string.winkSmiley, 
								R.string.winkSmiley,  R.string.amazedSmiley, R.string.amazedSmiley, R.string.amazedSmiley, R.string.kissSmiley, R.string.kissSmiley};
		String[]  smilesSymb = {":-)",  ":)", ":-(", ":(", ";-)",";)", ":-O", ":-o", ":-O", "(K)", "(k)"};	
			
		prepared = prepared.replace("...", res.getString(R.string.suspensionPoints));
		prepared = prepared.replace("facebook", res.getString(R.string.feisbuk));
		prepared = prepared.replace("Facebook", res.getString(R.string.feisbuk));
		
		for (int i = 0; i < smilesSymb.length; i++){
			prepared = prepared.replace(smilesSymb[i], res.getString(smilesCode[i]));
		}
		
		
		return prepared;
	}	
	
	public static String getJSONProperty(JSONObject json, String property){
		String value = null;
		
		try {
			value = json.getString(property);
		} catch (Exception ex){
			// nada
		}
		
		return value;
	}

	public static Bitmap loadBitmap(String uri){
		Bitmap		bitmap = null;
		InputStream input = null;

		try {
			uri = Uri.decode(uri);
			HttpGet httpRequest = new HttpGet(uri);
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response = (HttpResponse) httpclient.execute(httpRequest);
			HttpEntity entity = response.getEntity();
			BufferedHttpEntity bufferedHttpEntity = new BufferedHttpEntity(entity);

			input = bufferedHttpEntity.getContent();
			bitmap = BitmapFactory.decodeStream(input);


		} catch (Exception e) {
			Log.w("HalaLog", "Error decoding image from URL: " + uri, e);
		} finally {
			Utils.closeQuietly(input);
		}

		return bitmap;
	}
	
	public static void closeQuietly(InputStream input) {
		try {
			input.close();
		} catch (Exception ex){
			Log.w(HalaLog.TAG, "Closing inputStream", ex);
		}	
	}

	public static void closeQuietly(Cursor cursor) {
		try {
			cursor.close();
		} catch (Exception ex){
			Log.w(HalaLog.TAG, "Closing cursor", ex);
		}	
	}
	
	
	public static String getLineSeparator(){
		String nl = System.getProperty("line.separator");
		
		if (Utils.isEmpty(nl)){
			nl = "\n";
		}
		
		return nl;
	}
	
}
