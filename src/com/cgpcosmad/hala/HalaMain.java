package com.cgpcosmad.hala;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.admob.android.ads.AdManager;
import com.admob.android.ads.AdManager.Gender;
import com.admob.android.ads.AdView;
import com.cgpcosmad.hala.background.AskForFriendPresenceTask;
import com.cgpcosmad.hala.background.IBackgroudTaskResultListener;
import com.cgpcosmad.hala.background.LoadFriendsTask;
import com.cgpcosmad.hala.background.ValidatingConnectionToMailServerTask;
import com.cgpcosmad.hala.dao.Database;
import com.cgpcosmad.hala.entities.Application;
import com.cgpcosmad.hala.entities.DataReceived;
import com.cgpcosmad.hala.entities.Friend;
import com.cgpcosmad.hala.entities.Session;
import com.cgpcosmad.hala.entities.Setup;
import com.cgpcosmad.hala.log.HalaLog;
import com.cgpcosmad.hala.utils.Utils;
import com.cgpcosmad.widget.SeekBarCustom;
import com.facebook.android.Facebook;
import com.facebook.android.LoginButton;
import com.facebook.android.SessionEvents.AuthListener;
import com.facebook.android.SessionEvents.LogoutListener;
import com.facebook.android.SessionListener;
import com.facebook.android.SessionStore;

/**
 * Ventana principal de la aplicación
 * @author aaaCarlos García. cgpcosmad@gmail.com 
 */
public class HalaMain extends Activity implements OnClickListener, IBackgroudTaskResultListener, Callback, SessionListener, AuthListener, LogoutListener { 
	private static final int TTS_DIALOG_CODE			= 400;
	private static final int TERM_OF_USE_DIALOG_CODE	= 401;

	private static final int MY_DATA_CHECK_CODE = 1976;
	private static final long MILLISECOND_HOUR = 60 * 60 * 1000;
	
	public static final int READING_MAIL_MESSAGE_CODE = 6;
	public static final int READING_FACEBOOK_MESSAGE_CODE  = 7;
	public static final int PLAYING_MESSAGE_CODE  		= 8;
	public static final int PLAYING_TEXT_MESSAGE_CODE  = 9;
	public static final int END_PLAY_MESSAGE_CODE  	  = 10;
	public static final int TTS_ERROR_MESSAGE_CODE 		= 11;
	

	private ImageButton cmd_email;
	private ImageButton cmd_facebook;
	private ImageButton cmd_fb_online;	

	private LoginButton loginFB;
	private CheckBox chkUnreaded;
	private Spinner  lstFriends;
	private ArrayAdapter<Friend> lstFriendsAdapter;
	private CheckBox chkTTSOn;
	
	private SeekBarCustom  fromTimestamp;
	private AdView 	 ad;
	private AlertDialog ttsServiceRunningDialog;
	private ToggleButton muteRunningDialog;
	
	private Handler  aHandler;
	private Session  session;
	private Database database;

	private String  serviceToUseClassName;
    

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.main);
		this.serviceToUseClassName = TTSService.class.getName();
		this.aHandler = new Handler(this);
		this.session  = Session.getInstance(this.getApplicationContext());
		this.session.setActivityHandler(aHandler);
		this.database = Database.getInstance();
			
		this.cmd_email 	   = (ImageButton) this.findViewById(R.id.cmd_email);
		this.cmd_facebook  = (ImageButton) this.findViewById(R.id.cmd_facebook);
		this.cmd_fb_online = (ImageButton) this.findViewById(R.id.cmd_online);
		this.lstFriends    = (Spinner) this.findViewById(R.id.friends);
		
		this.chkUnreaded = (CheckBox) this.findViewById(R.id.chkUnreaded);
		this.chkTTSOn	 = (CheckBox) this.findViewById(R.id.chkTTSOn);
		
		this.loginFB 	 = (LoginButton) findViewById(R.id.loginFB);
		this.fromTimestamp 	 = (SeekBarCustom) this.findViewById(R.id.fromTimestamp);
		this.ad 		 = (AdView) this.findViewById(R.id.ad);
		
		this.lstFriendsAdapter = new ArrayAdapter<Friend>(this.getApplicationContext(), android.R.layout.simple_spinner_item,  new ArrayList<Friend>(128));
		this.lstFriendsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		this.lstFriends.setAdapter(lstFriendsAdapter);		
		
		this.setupAdManager();
		this.resetFriendList();
		
		this.cmd_email.setOnClickListener(this);
		this.cmd_facebook.setOnClickListener(this);
		this.cmd_fb_online.setOnClickListener(this);
		
		this.loginFB.init(this, session.getFacebook(), Session.FACEBOOK_PERMISSIONS, this);

		
		chkTTSOn.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked){
					// Verificamos que esté instalada la librería TTS
					 try {
				        Intent checkIntent = new Intent();
				        checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
				        startActivityForResult(checkIntent, MY_DATA_CHECK_CODE);
					 } catch (ActivityNotFoundException ex){
						 Toast.makeText(HalaMain.this, getResources().getString(R.string.ttsKO), Toast.LENGTH_LONG).show();
					 }					
				}
			}
		});
		
		
		
		ValidatingConnectionToMailServerTask talk = new ValidatingConnectionToMailServerTask(this, session.getSetup());
		talk.execute();
		
		if (! session.getSetup().isTermOfUseAccepted()){
			showDialog(TERM_OF_USE_DIALOG_CODE);
		}
	}

    /*
     * @see android.app.Activity#onActivityResult(int, int, android.content.Intent)
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MY_DATA_CHECK_CODE) {
            if (resultCode != TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
            	Toast.makeText(this, this.getResources().getString(R.string.ttsKO), Toast.LENGTH_LONG).show();
            }
        }
    }
    
	/**
	 * Configuramos las preferencias de la publicidad
	 */
	private void setupAdManager(){
		try {
			AdManager.setAllowUseOfLocation(true);
			
			Setup setup = this.session.getSetup();
			if ("female".equalsIgnoreCase(setup.getAdGender())){
				AdManager.setGender(Gender.FEMALE);
			} else if ("male".equalsIgnoreCase(setup.getAdGender())){
				AdManager.setGender(Gender.MALE);
			}
		} catch (Exception ex){
			Log.e(HalaLog.TAG, ex.toString());
		}		
	}

	@Override
	protected void onResume() {
		super.onResume();
		this.database.open(this.getApplicationContext());
		this.ad.requestFreshAd();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		Utils.stopService(getApplicationContext(), TTSService.class.getName());
		Utils.stopService(getApplicationContext(), TTSManualService.class.getName());
		
		this.session.close();
		this.database.close();	
	}	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_menu, menu);
		return true;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ( keyCode == KeyEvent.KEYCODE_BACK) {
			this.finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * Sugerir
	 */
	public void suggestClick(){
		Facebook facebook = Session.getInstance(this).getFacebook();
	    Bundle	 params	  = new Bundle();
	    params.putString("message", this.getString(R.string.suggestBody));
		facebook.dialog(this, "stream.publish", params, new DummyDialogListener());
	}
	
	public void rateClick(){
	    Uri		uriUrl		  = Uri.parse("https://market.android.com/details?id=com.cgpcosmad.hala");  
	    Intent	launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);  
	    startActivity(launchBrowser);  
	}
	
	/* 
	 * El usuario ha hecho clic en una de las opciones del menú
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		try {
			switch (item.getItemId()) {
				case R.id.suggest: {
					this.suggestClick();
					return true;
				}
				case R.id.setup: {
					startActivity(new Intent(this.getApplicationContext(), SetupActivity.class));
					return true;
				}
				
				case R.id.rate: {
					this.rateClick();
				}
			}
		} catch (Exception ex){
			// Pendiente
		}
		return false;
	}	

	@Override
	public void onClick(View v) {
		boolean isFacebook = false;
		boolean isEmail    = false;
		
		try {
			
			if (chkTTSOn.isChecked()){
				this.serviceToUseClassName = TTSService.class.getName();
			} else {
				this.serviceToUseClassName = TTSManualService.class.getName();
			}
			
			
			isEmail	   = (v == cmd_email);
			isFacebook = (v == cmd_facebook); 
			
			if (v == cmd_fb_online) {
				if (! session.getFacebook().isSessionValid()){
					Toast.makeText(this.getApplicationContext(), this.getResources().getString(R.string.facebookSessionError), Toast.LENGTH_LONG).show();
				} else {
					AskForFriendPresenceTask talk = new AskForFriendPresenceTask(this, session.getFacebook());
					talk.execute();
				}
				
				return;
			}
			
			if (isEmail){
				// Si no es valida mostramos el error y salimos
				if (! session.isEmailAccountValid()){
					Toast.makeText(this.getApplicationContext(), this.getResources().getString(R.string.connectingMailServerFail), Toast.LENGTH_LONG).show();
					return;
				}				
			}
			
			if (isFacebook) {
				if (! session.getFacebook().isSessionValid()){
					Toast.makeText(this.getApplicationContext(), this.getResources().getString(R.string.facebookSessionError), Toast.LENGTH_LONG).show();
					return;
				}
			}

			session.setSpeakEmailEnabled(isEmail);
			session.setSpeakFacebookEnabled(isFacebook);


			int fromTime   = this.fromTimestamp.getProgress();
			long timestamp = System.currentTimeMillis() - (fromTime * MILLISECOND_HOUR);


			if (! this.chkUnreaded.isChecked()){
				database.deleteFrom(timestamp, null);
			}
			
			if (isEmail) {
				session.setTapSpeakEmailTimestamp(timestamp);
			}

			if (isFacebook) {
				session.setTapSpeakFacebookTimestamp(timestamp);
			}

			Friend selected = (Friend) lstFriends.getSelectedItem();
			session.setFriendUID(selected.getUid());
			session.setUid(((Friend) lstFriends.getItemAtPosition(0)).getUid());
			
			showDialog(HalaMain.TTS_DIALOG_CODE);
			
			if (serviceToUseClassName.equalsIgnoreCase(TTSManualService.class.getName()) ){
				muteRunningDialog.setVisibility(View.GONE);
			} else {
				muteRunningDialog.setVisibility(View.VISIBLE);
			}
			
			

			clearDialog();
			
			if (! Utils.isServiceRunning(this, serviceToUseClassName)){
				Utils.startService(this, serviceToUseClassName);
			} else {
				boolean isServiceRunning = Session.getInstance(this).isServiceRunning();
				if (! isServiceRunning) {
					Utils.stopService(this,  serviceToUseClassName);
					Utils.startService(this, serviceToUseClassName);
				}
			}
		} catch (Exception ex){
			String message = ex.getMessage();
			if (Utils.isEmpty(message)){
				message = ex.getClass().getName(); // Da un error por qué NullPointerException no tiene message
			}
			
			Log.e(HalaLog.TAG, message);
			Utils.showMessageDialog(HalaMain.this, message);
		} finally {
			this.ad.requestFreshAd();
		}
	} 

	private void clearDialog() {
		final ViewGroup	parent = (ViewGroup) ttsServiceRunningDialog.findViewById(R.id.contentPanel);
		DataReceivedRender.paint(parent, new Application(""), getResources());
	}

	@SuppressWarnings("unchecked")
	@Override
	public void backgroundTaskFinished(String code, Object result) {
		if (ValidatingConnectionToMailServerTask.CODE.equals(code)){
			session.setEmailAccountValid((Boolean)result);	
		} else if (LoadFriendsTask.CODE.equals(code)){
			this.lstFriendsAdapter.clear();
			for (Friend friend :  (ArrayList<Friend>) result){
				this.lstFriendsAdapter.add(friend);	
			}
			lstFriends.setEnabled(true);			
		}
	}

	@Override
	public boolean handleMessage(Message msg) {
		boolean		 result = true;
		Bundle		 params = msg.getData();
		CharSequence text 	= null;
		
		try {
			final Button	uiNext = (Button)	 ttsServiceRunningDialog.findViewById(R.id.next);
			final ViewGroup	parent = (ViewGroup) ttsServiceRunningDialog.findViewById(R.id.contentPanel);
			
			uiNext.setEnabled((msg.what == HalaMain.PLAYING_MESSAGE_CODE) || (msg.what == HalaMain.PLAYING_TEXT_MESSAGE_CODE));
			
			
			if (msg.what == HalaMain.END_PLAY_MESSAGE_CODE){
				ttsServiceRunningDialog.hide();
			} else if (msg.what == HalaMain.PLAYING_TEXT_MESSAGE_CODE){
				final DataReceived data   = (DataReceived) params.getSerializable("object");
				final Bitmap	   bitmap = (Bitmap) params.getParcelable("attach_snapshot");
				
				if (data.getAttachment() != null){
					data.getAttachment().setBitmap(bitmap);
				}
				
				
				this.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						DataReceivedRender.paint(parent, data, getResources());
					}
				});
				
				return true;
				
			} else if (msg.what == HalaMain.PLAYING_MESSAGE_CODE){
				text = getText(R.string.playingState);
			} else if (msg.what == HalaMain.READING_FACEBOOK_MESSAGE_CODE){
				text = getText(R.string.readingFBState);
			} else if (msg.what == HalaMain.READING_MAIL_MESSAGE_CODE){
				text = getText(R.string.readingEmailState);
			} else if (msg.what == HalaMain.TTS_ERROR_MESSAGE_CODE){	
				text = getText(R.string.ttsInitError);
			} else {
				result = false;
			}
			
			
			if (text != null){
				DataReceivedRender.paint(parent, new Application(text.toString()), getResources());
			}
			
		} catch (Exception ex){
			String message = ex.getMessage();
			if (Utils.isEmpty(message)){
				message = ex.getClass().getName(); // Da un error por qué NullPointerException no tiene message
			}
			
			Log.e(HalaLog.TAG, message);
			Utils.showMessageDialog(HalaMain.this, message);
			result = false;
		}
		
		return result;
	}


    @Override
    protected Dialog onCreateDialog(int id) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		
    	if (id == HalaMain.TTS_DIALOG_CODE){
	    	LayoutInflater factory = LayoutInflater.from(this);
			View	   		body   = factory.inflate(R.layout.speaking_dialog, null);
			View	   		title  = factory.inflate(R.layout.speaking_dialog_title, null);

			
			builder.setView(body);
			builder.setTitle(R.string.applicationName);
			builder.setCustomTitle(title);
			
			builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Utils.stopService(HalaMain.this, serviceToUseClassName);
				}
			});
	
			Button cmdNext = (Button) title.findViewById(R.id.next);
			cmdNext.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (session.getServiceHandler() != null){
						session.getServiceHandler().sendEmptyMessage(TTSManualService.CANCEL_CURRENT_MESSAGE_CODE); // TODO: Sacarlo a una constante común.
					}					
				}
			});
			
			
			this.muteRunningDialog		 = (ToggleButton) title.findViewById(R.id.mute);
			this.ttsServiceRunningDialog =  builder.create();
			
			this.muteRunningDialog.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					AudioManager audio   = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
					if (audio != null){
						if (isChecked){
							audio.setStreamMute(AudioManager.STREAM_MUSIC, true);	
						} else {
							audio.setStreamMute(AudioManager.STREAM_MUSIC, false);
						}
					}
				}
			});
			
			ttsServiceRunningDialog.setOnKeyListener(new OnKeyListener() {
				@Override
				public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
					if (keyCode == KeyEvent.KEYCODE_BACK) {
						Utils.stopService(HalaMain.this, serviceToUseClassName);
						return true;
					}
					return false;
				}
			});
			
			ttsServiceRunningDialog.setCancelable(true);
			ttsServiceRunningDialog.setOnCancelListener(new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					Utils.stopService(HalaMain.this, serviceToUseClassName);
					
				}
			});
			
			return ttsServiceRunningDialog;
			
    	} else if (id == HalaMain.TERM_OF_USE_DIALOG_CODE){
			builder.setTitle(R.string.termsOfUseDialogTitle);
			builder.setMessage(R.string.termsOfUseDialog);
			builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					session.getSetup().setTermOfUseAccepted(false);
					finish();	// El usuario no acepto las condiciones de uso
				}
			});
			
			builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					session.getSetup().setTermOfUseAccepted(true);
				}
			});
			
			
    		return builder.create();
    	} else {
    		return null;
    	}
    }
    
    
	private void initFriendList()   {
        Toast.makeText(this.getApplicationContext(), R.string.loadingFriends, Toast.LENGTH_LONG).show();
		LoadFriendsTask talk = new LoadFriendsTask(this, session.getFacebook());
		talk.execute();
	}
	
    public void onAuthSucceed() {
        this.loginFB.setImageResource(R.drawable.logout_button);
        SessionStore.save(this.session.getFacebook(), this.getApplicationContext());
		initFriendList();	
    }

    public void onAuthFail(String error) {}
    
    public void onLogoutBegin() {}
    
    public void onLogoutFinish() {
        SessionStore.clear(this.getApplicationContext());
        this.loginFB.setImageResource(R.drawable.login_button);
        this.resetFriendList();
    }    
    
    private void resetFriendList(){
        this.lstFriendsAdapter.clear();
        this.lstFriendsAdapter.add(new Friend("", getText(R.string.friendList).toString()));
    }
}
