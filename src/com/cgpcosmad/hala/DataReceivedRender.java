package com.cgpcosmad.hala;

import android.content.res.Resources;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cgpcosmad.hala.entities.Application;
import com.cgpcosmad.hala.entities.Attachment;
import com.cgpcosmad.hala.entities.DataReceived;
import com.cgpcosmad.hala.entities.FacebookFeed;
import com.cgpcosmad.hala.entities.FacebookMail;
import com.cgpcosmad.hala.entities.Mail;
import com.cgpcosmad.hala.log.HalaLog;

public class DataReceivedRender {
	
	public static void paint(ViewGroup parent, DataReceived item, Resources resources){
		try {
			parent.removeAllViews();
			
			if (item instanceof Application){
				paint(parent,  (Application) item);	
			} else if (item instanceof Mail){
				paint(parent,  (Mail) item);
			} else if (item instanceof FacebookMail){
				paint(parent,  (FacebookMail) item);
			} else if (item instanceof FacebookFeed){
				paint(parent,  (FacebookFeed) item, resources);
			}
			
			parent.refreshDrawableState();
		} catch (Exception ex){
			Log.d(HalaLog.TAG, ex.toString());
		}
	}
	
	private  static void paint(ViewGroup parent, Application item){
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		View		   layout   = inflater.inflate(R.layout.render_application_template, parent, true);
		TextView	   view	    = (TextView) layout.findViewById(R.id.text);
		
		view.setText(item.getBody());
	}
	
	
	private  static void paint(ViewGroup parent, Mail item){
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		View	  layout = inflater.inflate(R.layout.render_mail_template, parent, true);
		TextView  uiText = (TextView) layout.findViewById(R.id.text);
		TextView  uiFrom = (TextView)  layout.findViewById(R.id.metaFrom);
		
		uiFrom.setText(item.getFrom());
		uiText.setText(Html.fromHtml(item.getBody()));
		uiText.setAutoLinkMask(Linkify.ALL);
		uiText.setMovementMethod(LinkMovementMethod.getInstance());
	}	
	
	
	private  static void paint(ViewGroup parent, FacebookMail item){
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		View	  layout = inflater.inflate(R.layout.render_facebook_mail_template, parent, true);
		TextView  uiText = (TextView) layout.findViewById(R.id.text);
		TextView  uiFrom = (TextView)  layout.findViewById(R.id.metaFrom);

		uiFrom.setText(item.getFrom());
		uiText.setText(item.getBody());
		uiText.setAutoLinkMask(Linkify.ALL);
		uiText.setMovementMethod(LinkMovementMethod.getInstance());
	}
	
	private  static void paint(ViewGroup parent, FacebookFeed item, Resources resources){
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		View	  layout 	    = inflater.inflate(R.layout.render_facebook_template, parent, true);
		View	  attachPanel   = (LinearLayout) layout.findViewById(R.id.attachmentPanel);
		TextView  uiText 	    = (TextView)  layout.findViewById(R.id.text);
		TextView  uiFrom 	    = (TextView)  layout.findViewById(R.id.metaFrom);
		TextView  uiType 	    = (TextView)  layout.findViewById(R.id.metaType);
		ImageView uiSnapshot    = (ImageView) layout.findViewById(R.id.snapshot);
		TextView  uiAttachSrc   = (TextView)  layout.findViewById(R.id.attach_src);
		
		Attachment attachment   = item.getAttachment();
		
		uiFrom.setText(item.getFrom());
		uiText.setText(item.getBody());
		
		
		if (item.isComment()){
			uiType.setText(resources.getString(R.string.fbCommentType));
		} else {
			uiType.setText(resources.getString(R.string.fbFeedType));
		}
		
		if (attachment != null){
			attachPanel.setVisibility(View.VISIBLE);
			
			uiAttachSrc.setText(Html.fromHtml("<a href='"+ attachment.getSrc()  +"'>(" + i18nType(attachment.getType(), resources) + ")</a>"));
			uiAttachSrc.setMovementMethod(LinkMovementMethod.getInstance());
	        
			if (attachment.getBitmap() == null){
				uiSnapshot.setImageResource(R.drawable.image_error);
			} else {
				attachment.getBitmap().prepareToDraw();
				uiSnapshot.setImageBitmap(attachment.getBitmap());	
			}
		} else {
			attachPanel.setVisibility(View.GONE);
		}
	}	
	
	private static String i18nType(String type, Resources resources){
		String i18n = null;
		
		if ("photo".equalsIgnoreCase(type)){
			i18n = resources.getString(R.string.photoAttachmentType);
		} else if ("video".equalsIgnoreCase(type)){
			i18n = resources.getString(R.string.videoAttachmentType);
		} else if ("link".equalsIgnoreCase(type)){
			i18n = resources.getString(R.string.linkAttachmentType);
		} else {
			i18n = type;
		}
		
		return i18n;
	}
	
}
