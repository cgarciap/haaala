package com.cgpcosmad.hala.dao;


import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.cgpcosmad.hala.R;
import com.cgpcosmad.hala.entities.DataReceived;
import com.cgpcosmad.hala.log.HalaLog;
import com.cgpcosmad.hala.utils.Utils;

public class Database  {
	private SQLiteDatabase db;
	private static Database		  instance;
	
	private Database(){}
	
	public static Database getInstance(){
		if ((Database.instance == null)){
			Database.instance = new Database();
		}
		return instance;
	}

	public void open(Context context){
		try {
			if ((db == null) || (! db.isOpen())){
				Resources resources = context.getResources();
				String    DB_NAME   = resources.getString(R.string.sqlDatabaseName);
				
				// Abrimos la base de datos creandola si no existiese
				db		  = context.openOrCreateDatabase(DB_NAME, SQLiteDatabase.CREATE_IF_NECESSARY, null);
		
				// Creamos la tabla si no existia ya
				db.execSQL(resources.getString(R.string.sqlCreateTableFiles));
			}
		} catch (SQLiteException ex){
			Log.e(HalaLog.TAG, ex.toString());
		}
	}
	
	public boolean isOpen(){
		try {
			return db.isOpen();
		} catch (Exception ex){
			return false;
		}
	}
	
	public void close(){
		try {
			db.close();
		} catch (Exception ex){
			// nada
		}
	}
	
	public boolean contains(String id){
		Cursor  cursor = null;
		boolean exists = false;
		
		try {
			cursor = db.rawQuery("SELECT id from already_spoken where id='" +  id + "'", null);
			exists = cursor.moveToFirst();
		} catch (Exception ex){
			// no debería producirse
		} finally {
			if (cursor != null){
				cursor.close();
			}
		}
		
		return exists;
	}

	
	public void add(DataReceived message) throws java.io.IOException {
		ContentValues values = this.getContentValues(message);
		long res = db.insert("already_spoken", null, values);
		
		if (res == -1){
			throw new java.io.IOException();
		}
	}	
	
	
	public long getMostRecentTimestamp(String mtype){
		Cursor  cursor = null;
		long	ctime  = 0;
		String	sql	   = "SELECT MAX(ctime) FROM already_spoken ";
		
		try {
			if (Utils.isNotEmpty(mtype)){
				sql	 = sql + " where mtype='" + mtype + "'";
			}
			
			
			cursor = db.rawQuery(sql, null);
			if (cursor.moveToFirst()){
				ctime = cursor.getLong(0);
			}
		} catch (Exception ex){
			// no debería producirse
		} finally {
			if (cursor != null){
				cursor.close();
			}
		}
		
		return ctime;
	}
	
	
	public int deleteFrom(long ctime, String mtype) {
		StringBuilder where = new StringBuilder(128);
		
		where.append("ctime >= " + ctime);
		if (Utils.isNotEmpty(mtype)){
			where.append(" and mtype='");
			where.append(mtype);
			where.append("'");
		}
		
		return db.delete("already_spoken", where.toString(), null);
	}
	
	private ContentValues getContentValues(DataReceived message){
		ContentValues values = new ContentValues(4);
		values.put("id",    message.getId());
		values.put("mtype", message.getType());
		values.put("ctime", message.getCtime());
		return values;
	}
}
