package com.cgpcosmad.hala;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;

import org.json.JSONObject;

import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;

import com.cgpcosmad.hala.dao.Database;
import com.cgpcosmad.hala.entities.Application;
import com.cgpcosmad.hala.entities.Attachment;
import com.cgpcosmad.hala.entities.DataReceived;
import com.cgpcosmad.hala.entities.FacebookFeed;
import com.cgpcosmad.hala.entities.FacebookMail;
import com.cgpcosmad.hala.entities.Mail;
import com.cgpcosmad.hala.entities.Session;
import com.cgpcosmad.hala.entities.Setup;
import com.cgpcosmad.hala.log.HalaLog;
import com.cgpcosmad.hala.utils.FacebookUtils;
import com.cgpcosmad.hala.utils.InboxEmailUtils;
import com.cgpcosmad.hala.utils.Utils;
import com.facebook.android.FacebookError;

/**
 * Servicio de monitorización/reproducción de email/facebook
 * @author Carlos García. cgpcosmad@gmail.com 
 */
public class TTSManualService extends android.app.Service implements Callback, Runnable, SensorEventListener { 

	public static final int    CANCEL_CURRENT_MESSAGE_CODE  = 3;
	
	private Handler 	 svcHandler;
	private Handler		 actHandler;
	private Setup		 setup;
	private Session		 session;
	private Thread       thread;

	private SensorManager mSensorManager;
    private Sensor		  sensor;	
	
	private Semaphore ttsSpeakFinishLock;
	private ArrayList<DataReceived>  received;

	private DataReceived currentSpeaking;
	private Database database;
	
	@Override
	public void onDestroy() {
		try {
			thread.interrupt();
		} catch (Exception ex){/* nada */}
		
		session.setServiceRunning(false);
		
		super.onDestroy();
	}

	
	@Override
	public void onStart(Intent intent, int startId) {
		if (startId > 1){
			return;
		}
		
		this.session  			= Session.getInstance(this.getApplicationContext());
		this.setup	  			= session.getSetup();
		this.actHandler			= session.getActivityHandler();
		this.ttsSpeakFinishLock = new Semaphore(0, true);		
		this.svcHandler 		= new Handler(this);
		this.received 	   		= new ArrayList<DataReceived>(32);
		this.database		  	= Database.getInstance();
		this.thread   	   		= new Thread(this);
	
		this.session.setServiceHandler(this.svcHandler);		
		this.thread.start();
	}
	
	@Override
	public android.os.IBinder onBind(android.content.Intent arg0) {return null;}

	
	public void run(){
		WakeLock 	 wl 	 = null;
	    KeyguardLock keyLock = null;
	    Resources	 res	 = this.getResources();
	    
		try {
			session.setServiceRunning(true);
			
			// Si no está habilitada la reproducción de algún item salimos
			if (session.isSpeakEmailEnabled() || session.isSpeakFacebookEnabled()){
			
				PowerManager	pm = (PowerManager)   this.getSystemService(Context.POWER_SERVICE);
				KeyguardManager km = (KeyguardManager)this.getSystemService(Context.KEYGUARD_SERVICE);
				
				wl		= pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, HalaLog.TAG);
				keyLock = km.newKeyguardLock(HalaLog.TAG);
				wl.acquire();	
				keyLock.disableKeyguard();
				
				this.received.add(new Application("")); // Se reemplazará más abajo
				
				// Si desea tratar los emails y hay conexión a internet...
				if (session.isSpeakEmailEnabled() && (Utils.getInetAddress() != null)){
			        actHandler.sendEmptyMessage(HalaMain.READING_MAIL_MESSAGE_CODE);
			        
					try {
						ArrayList<Mail> mails = InboxEmailUtils.getUnreadedEmails(this.getApplicationContext(), session.getTapSpeakEmailTimestamp(), database, setup);
						if (mails != null){
							this.received.addAll(mails);
						}	
					} catch (Exception e) {
						received.add(new Application(res.getString(R.string.emailErrorBody)));
					}
				}
				
				// Si desea tratar facebook y hay conexión a internet...
				if (session.isSpeakFacebookEnabled() && (session.getFacebook() != null) && session.getFacebook().isSessionValid() && (Utils.getInetAddress() != null)){
					actHandler.sendEmptyMessage(HalaMain.READING_FACEBOOK_MESSAGE_CODE);
					
					// Leemos los datos necesarios para propositos publicitaros la proxima vez que use la aplicación los tendrá en cuenta.
					try {
						// Leemos el genero 
						if (Utils.isEmpty(this.setup.getAdGender())){
							JSONObject json = FacebookUtils.getBirthdayAndSex(this.getApplicationContext());
							if (json != null){
								if (Utils.isEmpty(this.setup.getAdGender()) && json.has("sex")){
									this.setup.setAdGender(json.getString("sex"));
								}
							}
						}	
					} catch (Exception e) {
						Log.e(HalaLog.TAG,  e.getLocalizedMessage());
					}
					
					
					Throwable fbe = null;
					
					try {
						ArrayList<FacebookFeed> feeds = FacebookUtils.getFeeds(this.getApplicationContext(), session.getFriendUID(), session.getTapSpeakFacebookTimestamp(), database);
						if (feeds != null){
							this.received.addAll(feeds);
						}
						
						if (session.getUid().equalsIgnoreCase(session.getFriendUID())){
							ArrayList<FacebookMail> fbMails = FacebookUtils.getInbox(this.getApplicationContext(), session.getTapSpeakFacebookTimestamp(), database);
							if (fbMails != null){
								this.received.addAll(fbMails);
							}
						}
						
					} catch (Exception e) {
						fbe = e;
					} catch (FacebookError e) {
						fbe = e;
					}
					
					if (fbe != null){
					    Log.e(HalaLog.TAG, "error", fbe);
						received.add(new Application(res.getString(R.string.fbErrorBody)));
					}
				}
				
				int numMessages = isThereSomeToSpeak();
				if (numMessages == 0){
					received.set(0, new Application(this.getResources().getString(R.string.noMessages)));
				} else {
					received.set(0, new Application(this.getResources().getString(R.string.numMessagesToPlay) + " " + numMessages));
					received.add(new Application(this.getResources().getString(R.string.playEnd)));
				}
				
	
		        this.mSensorManager = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
		        if (this.mSensorManager != null){ // Si no existe retornará null
		        	this.sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		        	if (this.sensor != null) {
		        		mSensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
		        	}
		        }
		        
				actHandler.sendEmptyMessage(HalaMain.PLAYING_MESSAGE_CODE);
				
				for (int i = 0, num = received.size(); i < num; i++){
					DataReceived data = received.get(i);
					
					// Hablo y registro un listener para cuando termine
					this.currentSpeaking = data;
							
					
					// Anotamos el elemento reproducido para evitar reproducirlo de nuevo.
					if ((! (currentSpeaking instanceof Application)) && ! database.contains(currentSpeaking.getId())){
						try {
							database.add(currentSpeaking);
						} catch (IOException e) {
							Log.e(HalaLog.TAG, e.toString());	// No debería de pasar nunca por aqui
						}
					}
					
					Message message   = new Message();
					Bundle  params    = new Bundle();
					message.what	  = HalaMain.PLAYING_TEXT_MESSAGE_CODE;
					Attachment attach = currentSpeaking.getAttachment();
					params.putSerializable("object", currentSpeaking);
					
					if ((attach != null) && (attach.getBitmap() != null)){
						params.putParcelable("attach_snapshot", attach.getBitmap());
					}
					
					message.setData(params);
					actHandler.sendMessage(message);
					
					// Bloqueo el hilo hasta que termine de hablar
					ttsSpeakFinishLock.acquire();
				} // end for
				
				received.clear();
			}
		} catch (InterruptedException e) {
				// El usuario cancela la reproducción => todo ok
		} catch (Exception e) {
			Log.d(HalaLog.TAG, e.toString());
		} finally {
			session.setServiceRunning(false);
			
			
			// Ya no queremos notificaciones de sensores
			if (mSensorManager != null){
				mSensorManager.unregisterListener(this);
				mSensorManager = null;
			}
			
			// Podemos entrar en modo suspensión
			if (wl != null){
				wl.release();
			}

			if (keyLock != null) {
				keyLock.reenableKeyguard();
			}
			
			this.session.getActivityHandler().sendEmptyMessage(HalaMain.END_PLAY_MESSAGE_CODE);
			
			this.stopSelf();
		}
	}
	
	private int isThereSomeToSpeak(){
		int total = 0;	 
		
		if (this.received != null){
			for (DataReceived current: this.received){
				if (! (current instanceof Application)){ 
					total++;
				}
			}
		}
		return total;
	}
	

	/* 
	 * Se ha terminado de hablar todo
	 * @see android.speech.tts.TextToSpeech.OnUtteranceCompletedListener#onUtteranceCompleted(java.lang.String)
	 */
	public void onUtteranceCompleted(String id) {
		// Indicamos que hemos terminado
		ttsSpeakFinishLock.release();
}  
	
	/**
	 * Cancela la reproducción actual (Si no es un mensaje de aplicación)
	 * Sólo si está bloqueado el semaforo que implica que no terminó la reproducción aun, y así evitamos
	 * cancelar la siguiente reproducción.
	 */
	private void cancelCurrent() {
		if (currentSpeaking != null) {
			onUtteranceCompleted(currentSpeaking.getId());
		}
	}	
	
	@Override
	public boolean handleMessage(Message msg) {
		if (msg.what == TTSManualService.CANCEL_CURRENT_MESSAGE_CODE){
			this.cancelCurrent();
		}
		return true;
	}
	
	
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {}

	@Override
	public void onSensorChanged(SensorEvent event) {
		float[] values = event.values;
		
		if ((values[0] > 15) || (values[1] > 15)){
			this.cancelCurrent();
		}
	}	
}
